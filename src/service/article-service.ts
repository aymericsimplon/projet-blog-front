import type {ArticleEssai} from '@/entities'
import axios from 'axios'

export async function fetchArticle() {
    const response = await axios.get<ArticleEssai[]>('http://localhost:8080/api/article')
    return response.data
}
export async function fetchArticleOfCategorie(id:any) {
    const response = await axios.get<ArticleEssai[]>('http://localhost:8080/api/article/categorie/'+id)
    return response.data
}

export async function fetchOneArticle(id:any) {
    const response = await axios.get<ArticleEssai>('http://localhost:8080/api/article/'+id);
    return response.data;
}

export async function postArticle(article:ArticleEssai) {
    const response = await axios.post<ArticleEssai>('http://localhost:8080/api/article', article);
    return response.data;
}

export async function deleteArticle(id:any) {
    await axios.delete<void>('http://localhost:8080/api/article/'+id);
}

export async function updateArticle(article:ArticleEssai) {
    const response = await axios.put<ArticleEssai>('http://localhost:8080/api/article/'+article.id, article);
    return response.data;
}

