import type {ArticleEssai, CategoryInterface} from '@/entities'
import axios from 'axios'

export async function fetchCategorie() {
    const response = await axios.get<CategoryInterface[]>('http://localhost:8080/api/categorie')
    return response.data
}


export async function fetchOneCategorie(id:any) {
    const response = await axios.get<CategoryInterface>('http://localhost:8080/api/categorie/' +id)
    return response.data
}
