export interface ArticleEssai {
    id?:number,
    title: string,
    url: string,
    content: string
}

export interface CategoryInterface {
    id?:number,
    name: string,
}

export interface CommentairesInterface {
    id?:number,
    name: string,
    date: string,
    content: string
}

export interface User {
    id?:number,
    name: string,
    email: string,
    password: string,
    role: string
}