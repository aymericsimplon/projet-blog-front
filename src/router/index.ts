import AddArticleView from "@/views/AddArticleView.vue";
import Categorie from "@/views/Categorie.vue";
import HomeView from "@/views/HomeView.vue";
import LoginView from "@/views/LoginView.vue";
import SingleArticleView from "@/views/SingleArticleView.vue";
import { createRouter, createWebHistory } from "vue-router";



export const router  = createRouter({
    history: createWebHistory(),
    // linkActiveClass: 'active',
    routes: [
        {   
            path: '/',
            component: HomeView
        },
        {
            path: '/article/:id',
            component: SingleArticleView
        },

        {
            path: '/categorie/:id',
            name:  'categorie',
            component: Categorie
        },
        {
            path: '/add-article',
            name: 'add-article',
            component: AddArticleView
        },
        {
            path: '/login',
            name:  'login',
            component: LoginView
        },
    ]
});