import { createApp } from 'vue'
import App from './App.vue'
import './axios-config';
import { router } from './router'
import 'bootstrap/dist/css/bootstrap.css';
import { createPinia } from 'pinia';
import './assets/main.css';
import "./assets/lib/animate/animate.min.css";
import "./assets/lib/owlcarousel/assets/owl.carousel.min.css";
import "./assets/lib/lightbox/css/lightbox.min.css";





const app = createApp(App)


app.use(createPinia())
app.use(router)
app.mount('#app')
